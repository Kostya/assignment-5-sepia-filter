#include "sepia_api.h"
#include <math.h>

struct maybe_image  sepia(struct image const *image) {
    struct maybe_image new_image = initialize_image(image->height, image->width);
    if (new_image.valid == false) return new_image;
    
    uint64_t height = image->height;
    uint64_t width = image->width;
    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            struct pixel pixel = new_image.image.data[i * width + j];
            float sepia_red =
                    0.393 * pixel.r + 0.769 * pixel.g + 0.189 * pixel.b;
            float sepia_Green =
                    0.349 * pixel.r + 0.686 * pixel.g + 0.168 * pixel.b;
            float sepia_Blue =
                    0.272 * pixel.r + 0.534 * pixel.g + 0.131 * pixel.b;
            sepia_red = roundf(sepia_red);
            sepia_Green = roundf(sepia_Green);
            sepia_Blue = roundf(sepia_Blue);

            if (sepia_red > 255) {
                sepia_red = 255;
            }

            if (sepia_Green > 255) {
                sepia_Green = 255;
            }

            if (sepia_Blue > 255) {
                sepia_Blue = 255;
            }

            new_image.image.data[i * width + j].r = (uint8_t) sepia_red;
            new_image.image.data[i * width + j].g = (uint8_t) sepia_Green;
            new_image.image.data[i * width + j].b = (uint8_t) sepia_Blue;
        }
    }
    return new_image;
}
