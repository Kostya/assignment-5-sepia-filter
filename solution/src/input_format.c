#include "input_format.h"
#include <stdint.h>
#include <stdio.h>
/*  deserializer   */
#define BfType 19778
#define BfReserved 0
#define BOffBits sizeof(struct bmp_header)
#define BiSize 40
#define BiPlanes 1
#define BiBitCount 24
#define BiCompression 0
#define BiXPelsPerMeter 2834
#define BiYPelsPerMeter 2834
#define BiClrUsed 0
#define BiClrImportant 0
#define padding(x) ((4 - ((x)* 3) % 4) % 4)
/* SYSTEM_ERROR - code of malloc error
 * This code processed in main */
enum read_status from_bmp(FILE *in, struct image *img){
    struct bmp_header header = {0};
    if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_SIGNATURE;
    }

    struct maybe_image new_maybe_image = initialize_image(header.biWidth, header.biHeight);
    struct image new_image;
    if (new_maybe_image.valid) {
        new_image = new_maybe_image.image;
    } else {
        return SYSTEM_ERROR;
    }
    for (uint64_t i = 0; i < header.biHeight; i++) {
        for (uint64_t j = 0; j < header.biWidth; j++) {
            if (!fread(new_image.data + i * header.biWidth + j, sizeof(struct pixel), 1, in)) {
                return READ_INVALID_SIGNATURE;
            }
        }
        fseek(in, (long) padding(header.biWidth), SEEK_CUR);
    }
    *img = new_image;
    return READ_OK;
}

static enum write_status write_pixels(FILE *out, struct image const *img) {
    for (uint64_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + i * img->width, sizeof (struct pixel), img->width, out)) {
            return WRITE_ERROR;
        }
        int8_t nul = 0;
        for (uint64_t j = 0; j < padding(img->width); j++){
            if (!fwrite(&nul, 1, 1, out)) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = {0};
    header.bfType = BfType;
    header.biSizeImage = (sizeof(struct pixel) * img->width + padding(img->width)) * img->height;
    header.bfileSize = header.biSizeImage;
    header.bfReserved = BfReserved;
    header.bOffBits = BOffBits;
    header.biSize = BiSize;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BiPlanes;
    header.biBitCount = BiBitCount;
    header.biCompression = BiCompression;
    header.biXPelsPerMeter = BiXPelsPerMeter;
    header.biYPelsPerMeter = BiYPelsPerMeter;
    header.biClrUsed = BiClrUsed;
    header.biClrImportant = BiClrImportant;
    if (!fwrite(&header, sizeof (header), 1, out)) {return WRITE_ERROR;}
    write_pixels(out, img);
    return WRITE_OK;
}

void close(FILE *file);
