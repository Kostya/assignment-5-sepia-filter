#include "inner_format.h"
#include "input_format.h"
#include "transform_format.h"
#include "sepia_api.h"
#include <stdio.h>
#include <stdlib.h>

int open(struct image *image, int argc, char **argv) {
    (void) argc;
    (void) argv; // supress 'unused parameters' warning
    if (argc != 4) {
        fprintf(stderr, "wrong count variables");
        return -1;
    }
    FILE *file = fopen(argv[1], "rb");
    if (file == NULL) {
        fprintf(stderr, "File is not exist");
        return READ_WRONG_PATH;
    }
    switch (from_bmp(file, image)) {
        case READ_OK:
            break;
        case SYSTEM_ERROR:
            fprintf(stderr, "memory allocation error");
            return SYSTEM_ERROR;
        default:
            fprintf(stderr, "Bad picture");
            deinitialize_image(image);
            fclose(file);
            return READ_INVALID_SIGNATURE;
    }
    if (fclose(file) != 0) {
        fprintf(stderr, "Close error");
        return 4;
    }
    return 0;
}

int rotate_img(struct maybe_image *rotate_maybe_image, struct image *rotate_image, struct image *image, char **argv) {
    switch (strtol(argv[3], NULL, 10)) {
        case 0 :
            *rotate_maybe_image = rotate(image, ZERO);
            break;
        case 90:
        case -270:
            *rotate_maybe_image = rotate(image, QUARTER);
            break;
        case 180:
        case -180:
            *rotate_maybe_image = rotate(image, HALF);
            break;
        case 270:
        case -90:
            *rotate_maybe_image = rotate(image, MINUS_QUARTER);
            break;
        default:
            fprintf(stderr, "Wrong angle");
            deinitialize_image(image);
            return -1;
    }
    if (!rotate_maybe_image->valid) {
        fprintf(stderr, "memory allocation error");
        return SYSTEM_ERROR;
    } else {
        *rotate_image = rotate_maybe_image->image;
    }
    return 0;
}

int sepia_img(struct maybe_image *sepia_maybe_image, struct image *sepia_image, struct image *image) {
    *sepia_maybe_image = sepia(image);
    if (!sepia_maybe_image->valid) {
        fprintf(stderr, "memory allocation error");
        return SYSTEM_ERROR;
    } else {
        *sepia_image = sepia_maybe_image->image;
    }
    return 0;
}

int write(struct image* image, struct image* sepia_image, char **argv) {
    FILE* file = fopen(argv[2], "wb");
    if (file == NULL) {
        fprintf(stderr, "Wrong path for target file");
        deinitialize_image(image);
        deinitialize_image(sepia_image);
        return -1;
    }
    switch (to_bmp(file, sepia_image)) {
        case WRITE_OK:
            fprintf(stdout, "All is OK");
            deinitialize_image(image);
            deinitialize_image(sepia_image);
            if (fclose(file) != 0) {
                fprintf(stderr, "Close error");
                return 4;
            }
            return 0;
        case WRITE_ERROR:
            fprintf(stderr, "Wrong file");
            deinitialize_image(image);
            deinitialize_image(sepia_image);
            if (fclose(file) != 0) {
                fprintf(stderr, "Close error");
            }
            return WRITE_ERROR;
    }
}
