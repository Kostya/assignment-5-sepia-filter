#include "transform_format.h"
#include <stdint.h>
static struct maybe_image rotate0(struct image const * source){
    struct maybe_image new_image = initialize_image(source->width, source->height);
    if (new_image.valid == false) return new_image;
    for (uint64_t i = 0; i < source->height; ++i) {
        for (uint64_t j = 0; j < source->width; ++j) {
            new_image.image.data[i * source->width + j] = source->data[i * source->width + j];
        }
    }
    return new_image;
}

static struct maybe_image rotate90(struct image const * source){
    struct maybe_image new_image = initialize_image(source->height, source->width);
    if (new_image.valid == false) return new_image;
    for (uint64_t i = 0; i < source->height; ++i) {
        for (uint64_t j = 0; j < source->width; ++j) {
            new_image.image.data[source->height * (j + 1) - i - 1] = source->data[i * source->width + j];
        }
    }
    return new_image;
}

static struct maybe_image rotate180(struct image const * source){
    struct maybe_image new_image = initialize_image(source->width, source->height);
    if (new_image.valid == false) return new_image;
    for (uint64_t i = 0; i < new_image.image.height; ++i) {
        for (uint64_t j = 0; j < new_image.image.width; ++j) {
            new_image.image.data[source->height * source->width - 1 - source->width * i - j] = source->data[i * new_image.image.width + j];
        }
    }
    return new_image;
}

static struct maybe_image rotate270(struct image const * source){
    struct maybe_image new_image = initialize_image(source->height, source->width);
    if (new_image.valid == false) return new_image;
    for (uint64_t i = 0; i < new_image.image.height; ++i) {
        for (uint64_t j = 0; j < new_image.image.width; ++j) {
            new_image.image.data[i * new_image.image.width + j] = source->data[new_image.image.height * (j + 1) - i - 1];
        }
    }
    return new_image;
}

struct maybe_image rotate(struct image const * source, enum angle angle) {
    switch (angle) {
        case ZERO:
            return rotate0(source);
        case QUARTER:
            return rotate270(source);
        case HALF:
            return rotate180(source);
        default:
            return rotate90(source);
    }
}
