#include "bmp_api.h"

/* return value 4 - close error
 * return value 5 - close error */
int main( int argc, char** argv ) {
    int status;
    struct image image;
    status = open(&image, argc, argv);
    if (status) return status;

    struct maybe_image sepia_maybe_image;
    struct image sepia_image;
    status = sepia_img(&sepia_maybe_image, &sepia_image, &image);
    if (status) return status;

    status = write(&image, &sepia_image, argv);
    return status;
}
