#ifndef ASSIGNMENT_5_SEPIA_FILTER_BMP_API_H
#define ASSIGNMENT_5_SEPIA_FILTER_BMP_API_H

#include "inner_format.h"

int open(struct image *image, int argc, char **argv);
int rotate_img(struct maybe_image *rotate_maybe_image, struct image *rotate_image, struct image *image, char **argv);
int sepia_img(struct maybe_image *sepia_maybe_image, struct image *sepia_image, struct image *image);
int write(struct image* image, struct image* sepia_image, char **argv);

#endif //ASSIGNMENT_5_SEPIA_FILTER_BMP_API_H
