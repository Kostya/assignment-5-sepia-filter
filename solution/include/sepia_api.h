#ifndef ASSIGNMENT_5_SEPIA_FILTER_SEPIA_API_H
#define ASSIGNMENT_5_SEPIA_FILTER_SEPIA_API_H
#include "inner_format.h"

struct maybe_image sepia(struct image const* image);
#endif //ASSIGNMENT_5_SEPIA_FILTER_SEPIA_API_H
