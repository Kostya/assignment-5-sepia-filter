#ifndef IMAGE_TRANSFORMER_INNER_FORMAT_H
#define IMAGE_TRANSFORMER_INNER_FORMAT_H

#include <stdbool.h>
#include <stdint.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct maybe_image {
    struct image image;
    bool valid;
};

struct maybe_image create_maybe_image(void);
struct maybe_image initialize_image(uint64_t w, uint64_t h);
void deinitialize_image(struct image* img);
#endif //IMAGE_TRANSFORMER_INNER_FORMAT_H
