
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/mnt/c/Assembler/assignment-5-sepia-filter/solution/src/bmp_api.c" "CMakeFiles/image-transformer.dir/solution/src/bmp_api.o" "gcc" "CMakeFiles/image-transformer.dir/solution/src/bmp_api.o.d"
  "/mnt/c/Assembler/assignment-5-sepia-filter/solution/src/inner_format.c" "CMakeFiles/image-transformer.dir/solution/src/inner_format.o" "gcc" "CMakeFiles/image-transformer.dir/solution/src/inner_format.o.d"
  "/mnt/c/Assembler/assignment-5-sepia-filter/solution/src/input_format.c" "CMakeFiles/image-transformer.dir/solution/src/input_format.o" "gcc" "CMakeFiles/image-transformer.dir/solution/src/input_format.o.d"
  "/mnt/c/Assembler/assignment-5-sepia-filter/solution/src/main.c" "CMakeFiles/image-transformer.dir/solution/src/main.o" "gcc" "CMakeFiles/image-transformer.dir/solution/src/main.o.d"
  "/mnt/c/Assembler/assignment-5-sepia-filter/solution/src/sepia_api.c" "CMakeFiles/image-transformer.dir/solution/src/sepia_api.o" "gcc" "CMakeFiles/image-transformer.dir/solution/src/sepia_api.o.d"
  "/mnt/c/Assembler/assignment-5-sepia-filter/solution/src/transform_format.c" "CMakeFiles/image-transformer.dir/solution/src/transform_format.o" "gcc" "CMakeFiles/image-transformer.dir/solution/src/transform_format.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
